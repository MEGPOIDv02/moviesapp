//
//  UITableViewCellExtension.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//

import UIKit

extension UITableViewCell{
    //MARK: functions to remove html code in text
    func clearText (tex: String) -> String {
        var str = tex
        var start = 0, end = 0, conta = 0
        var arrayOfStrings: [String] = []

        str.forEach { i in
            if i == "<" {
               start = conta
            }
            else if i == ">" {
                end = conta
                let startR = str.index(str.startIndex, offsetBy: start)
                let endR = str.index(str.startIndex, offsetBy: end)
                let sub = str[startR...endR]
                arrayOfStrings.append(String(sub))
            }
            conta = conta + 1
        }//forEach

        arrayOfStrings.forEach { i in
            str = str.replacingOccurrences(of: i, with: "")
        }
        return str
    }
    
    func clearTextHTML (tex: String) -> String {
           var str = tex
           var start = 0, end = 0, conta = 0
           var arrayOfStrings: [String] = []

           str.forEach { i in
               if i == "&" {
                  start = conta
               }
               else if i == ";" {
                   end = conta
                   let startR = str.index(str.startIndex, offsetBy: start)
                   let endR = str.index(str.startIndex, offsetBy: end)
                   let sub = str[startR...endR]
                   arrayOfStrings.append(String(sub))
               }
               conta = conta + 1
           }//forEach

           arrayOfStrings.forEach { i in
               str = str.replacingOccurrences(of: i, with: "")
           }
           return str
       }
}
