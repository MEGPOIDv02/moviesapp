//
//  ViewControllerExtension.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//

import UIKit

extension UIViewController{
    
    //MARK: function style tab bar
    func setupViewWithTab(titleView: String) {
        self.setupView(titleView: titleView)
        UITabBar.appearance().barTintColor = .black
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
    }
    
    //MARK: function style navigation controller
    func setupView(titleView: String) {
        
        if #available(iOS 13.0, *) {
            UINavigationBar.appearance().backgroundColor = UIColor.systemIndigo
            UINavigationBar.appearance().barTintColor = UIColor.systemIndigo
            self.view.backgroundColor = UIColor.systemIndigo
        } else {
            UINavigationBar.appearance().backgroundColor = UIColor.purple
            UINavigationBar.appearance().barTintColor = UIColor.purple
            self.view.backgroundColor = UIColor.purple
        }
        
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
        self.title = titleView
    }
    
    
    //MARK: functions to remove html code in text
    
    func clearText (tex: String) -> String {
        var str = tex
        var start = 0, end = 0, conta = 0
        var arrayOfStrings: [String] = []

        str.forEach { i in
            if i == "<" {
               start = conta
            }
            else if i == ">" {
                end = conta
                let startR = str.index(str.startIndex, offsetBy: start)
                let endR = str.index(str.startIndex, offsetBy: end)
                let sub = str[startR...endR]
                arrayOfStrings.append(String(sub))
            }
            conta = conta + 1
        }//forEach

        arrayOfStrings.forEach { i in
            str = str.replacingOccurrences(of: i, with: "")
        }
        return str
    }
    
    func clearTextHTML (tex: String) -> String {
           var str = tex
           var start = 0, end = 0, conta = 0
           var arrayOfStrings: [String] = []

           str.forEach { i in
               if i == "&" {
                  start = conta
               }
               else if i == ";" {
                   end = conta
                   let startR = str.index(str.startIndex, offsetBy: start)
                   let endR = str.index(str.startIndex, offsetBy: end)
                   let sub = str[startR...endR]
                   arrayOfStrings.append(String(sub))
               }
               conta = conta + 1
           }//forEach

           arrayOfStrings.forEach { i in
               str = str.replacingOccurrences(of: i, with: "")
           }
           return str
       }
}
