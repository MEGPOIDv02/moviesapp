//
//  UIImageViewExtension.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//

import Kingfisher

extension UIImageView {
    //MARK: function to set image in UIViewController
    func setImage(fromUrl: String) {
        let url = URL(string: fromUrl)
        self.kf.setImage(with: url)
    }
    
    //MARK: function round image
    func makeRoundedWithoutBorder() {
        layer.masksToBounds = false
        layer.cornerRadius = self.frame.height / 2
        clipsToBounds = true
    }
}
