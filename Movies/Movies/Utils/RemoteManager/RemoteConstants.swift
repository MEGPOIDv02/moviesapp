//
//  RemoteConstants.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//

import Foundation

//MARK: URL Remote
enum ApiEndpoint: String {
    case shows = "https://api.tvmaze.com/shows"
    case imdb = "https://www.imdb.com/title/"
}
