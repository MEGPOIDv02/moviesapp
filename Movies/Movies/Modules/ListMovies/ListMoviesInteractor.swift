//
//  ListMoviesInteractor.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation

class ListMoviesInteractor: ListMoviesInteractorInputProtocol {
    weak var presenter: ListMoviesInteractorOutputProtocol?
     var service: ListMoviesServiceInputProtocol?
    
    func getShows() {
        service?.getShows()
    }
    
    func getFavorites() {
        service?.getFavorites()
    }
    
    func postAddFavorite(showData:Show){
        service?.postAddFavorite(showData:showData)
    }
    
    func postDeleteFavorite(showData:Show){
        service?.postDeleteFavorite(showData:showData)
    }
}


extension ListMoviesInteractor: ListMoviesServiceOutputProtocol {
    // TODO: Implement use case methods
    
    func requestShows(data: [Show]) {
        presenter?.requestShows(data: data)
    }
    
    func requestFavorites(data: [Show]) {
        presenter?.requestFavorites(data: data)
    }
    
    func responseServiceError(message: String) {
        presenter?.responseError(message: message)
    }
    
    func responseServiceSuccess(message: String) {
        presenter?.responseSuccess(message: message)
    }
} 
