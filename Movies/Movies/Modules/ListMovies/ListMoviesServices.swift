//
//  ListMoviesServices.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import UIKit
import CoreData

class ListMoviesService: ListMoviesServiceInputProtocol {
    weak var interactor: ListMoviesServiceOutputProtocol?
    
    //MARK: function getShows from remote API
    func getShows() {
        let jsonUrlString = ApiEndpoint.shows.rawValue
        //MARK: validate url
            guard let url = URL(string: jsonUrlString) else
            { return }
        
        //MARK: do getData
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data =  data else{
                    return
                }

                do {
                    //MARK: Decode response in Model Show
                    let shows = try JSONDecoder().decode([Show].self, from: data)
                    //MARK: function request send to interactor
                    self.interactor?.requestShows(data: shows)

                } catch {
                    //MARK: function request error in remote API
                    self.interactor?.responseServiceError(message: "Ocurrió un error al consultar el servicio. ¿Quieres intentar nuevamente?")
                }

            }.resume()
    }
    
    
    //MARK: Add favorite in local DB
    func postAddFavorite(showData: Show) {
        //MARK: use CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorite")
        request.returnsObjectsAsFaults = false
        do {
            //MARK: Check if exist Show in Favorites
            var isRepeatShow: Bool = false
            let result = try context.fetch(request)
                for data in result as! [NSManagedObject]{
                    if data.value(forKey: "id") as! Int == showData.id {
                        isRepeatShow = true
                    }
                }
            //MARK: if isRepeat send message else add show to Local DB
                if isRepeatShow {
                    interactor?.responseServiceError(message: "Este show ya esta en tus favoritos")
                }else{
                    //MARK: ADD new favorite
                    let entity = NSEntityDescription.entity(forEntityName: "Favorite", in: context)
                    let newFavorite = NSManagedObject(entity: entity!, insertInto: context)
                    
                    newFavorite.setValue(showData.id, forKey: "id")
                    newFavorite.setValue(showData.name, forKey: "name")
                    newFavorite.setValue(showData.fullGenres, forKey: "genres")
                    
                    if let average = showData.rating.average {
                        newFavorite.setValue(average, forKey: "average")
                    }
                    
                    newFavorite.setValue(showData.image.medium, forKey: "medium")
                    newFavorite.setValue(showData.image.original, forKey: "original")
                    newFavorite.setValue(showData.summary, forKey: "summary")
                    
                    if let imdb = showData.externals.imdb {
                        newFavorite.setValue(imdb, forKey: "imdb")
                    }
                    
                    do {
                        //MARK: try save & response message success
                      try context.save()
                         interactor?.responseServiceSuccess(message: "Se ha guardado tu show en favoritos")
                     } catch {
                         //MARK: response error add in favorites
                         interactor?.responseServiceError(message: "Hubo un problema al guardar este show de TV. ¿Quieres intentar nuevamente?")
                    }
                }

            } catch {
                //MARK: response error if fail get local data
                interactor?.responseServiceError(message: "Ocurrió un error al consultar el servicio. ¿Quieres intentar nuevamente?")
            }
        
        
        
    }
    
    //MARK: delete favorite in local DB
    func postDeleteFavorite(showData:Show){
        //MARK: use CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorite")
        
        request.returnsObjectsAsFaults = false
        do {
            //MARK: compare id to remove from local DB
            let result = try context.fetch(request)
                for data in result as! [NSManagedObject]{
                    if data.value(forKey: "id") as! Int == showData.id {
                        context.delete(data)
                    }
                }
            
                do {
                    //MARK: save changes local DB & send Message success
                    try context.save()
                    interactor?.responseServiceSuccess(message: "Se ha borrado tu show de favoritos")
                }catch {
                    //MARK: response error delete in favorites
                    interactor?.responseServiceError(message: "Hubo un problema al borrar este show de TV. ¿Quieres intentar nuevamente?")
                }
            } catch {
                //MARK: response error if fail get local data
                interactor?.responseServiceError(message: "Ocurrió un error al consultar el servicio. ¿Quieres intentar nuevamente?")
            }
    }
    
    //MARK: function get Favorites from local DB
    func getFavorites() {
        //MARK: use CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorite")
        request.returnsObjectsAsFaults = false
        do {
            //MARK: Transform data from DB to Model Show
            var arrayFavorites: [Show] = []
            
            let result = try context.fetch(request)
            
            for data in result as! [NSManagedObject] {
                let genresString = data.value(forKey: "genres") as! String
                let genresArray = genresString.components(separatedBy: "\n")
                
                let favorite = Show(
                    id: data.value(forKey: "id") as! Int,
                    name: data.value(forKey: "name") as! String,
                    genres: genresArray,
                    rating: Rating(average: data.value(forKey: "average") as? Float),
                    image: ImageBillboard(medium: data.value(forKey: "medium") as! String, original: data.value(forKey: "original") as! String),
                    summary: data.value(forKey: "summary") as! String,
                    externals: Externals(imdb: data.value(forKey: "imdb") as? String))
                
                arrayFavorites.append(favorite)
            }
            
            //MARK: send get Favorites to interactor
            interactor?.requestFavorites(data: arrayFavorites)

        } catch {
            //MARK: response error if fail get local data
            interactor?.responseServiceError(message: "Ocurrió un error al consultar el servicio. ¿Quieres intentar nuevamente?")
        }
    }
}
