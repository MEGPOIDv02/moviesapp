//
//  ListMoviesProtocols.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation

//View
protocol ListMoviesViewProtocol: AnyObject {
    var presenter: ListMoviesPresenterProtocol? {get set}
    func requestShows(data: [Show])
    func requestFavorites(data: [Show])
    func showError(text: String)
    func showSuccess(text: String)
}

//Interactor
protocol ListMoviesInteractorInputProtocol: AnyObject {
    //Presenter -> Interactor
    var presenter: ListMoviesInteractorOutputProtocol? {get set}
    var service: ListMoviesServiceInputProtocol? { get set }
    
    func getShows()
    func getFavorites()
    func postAddFavorite(showData:Show)
    func postDeleteFavorite(showData:Show)
}


protocol ListMoviesInteractorOutputProtocol: AnyObject {
    //Interactor->Presenter
    func requestShows(data: [Show])
    func requestFavorites(data: [Show])
    func responseError(message: String)
    func responseSuccess(message: String)
}

//Presenter
protocol ListMoviesPresenterProtocol: AnyObject {
    var interactor: ListMoviesInteractorInputProtocol? {get set}
    
    func getShows()
    func getFavorites()
    func goToDetails(showData:Show, delegate: DetailsMovieProtocol, isFavorite: Bool)
    func postAddFavorite(showData:Show)
    func postDeleteFavorite(showData:Show)
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void))
}


//Service
protocol ListMoviesServiceInputProtocol: AnyObject {
    // INTERACTOR -> SERVICE
    var interactor: ListMoviesServiceOutputProtocol? { get set }
    
    func getShows()
    func getFavorites()
    func postAddFavorite(showData:Show)
    func postDeleteFavorite(showData:Show)
}

protocol ListMoviesServiceOutputProtocol: AnyObject {
    // SERVICE -> INTERACTOR
    
    func requestShows(data: [Show])
    func requestFavorites(data: [Show])
    func responseServiceError(message: String)
    func responseServiceSuccess(message: String)
    
}

//Router
protocol ListMoviesRouterProtocol: AnyObject {
    func goToDetails(showData:Show, delegate: DetailsMovieProtocol, isFavorite: Bool)
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void))
}
