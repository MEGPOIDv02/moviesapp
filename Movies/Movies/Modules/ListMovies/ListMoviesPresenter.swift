//
//  ListMoviesPresenter.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation

class ListMoviesPresenter {
    //MARK: Properties
    var interactor: ListMoviesInteractorInputProtocol?
    weak private var view: ListMoviesViewProtocol?
    private let router: ListMoviesRouterProtocol
    
    //MARK: Dependency injection
    init(interface: ListMoviesViewProtocol, interactor: ListMoviesInteractorInputProtocol, router: ListMoviesRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
}

extension ListMoviesPresenter: ListMoviesPresenterProtocol {
    func getShows() {
        interactor?.getShows()
    }
    
    func getFavorites() {
        interactor?.getFavorites()
    }
    
    func goToDetails(showData:Show, delegate: DetailsMovieProtocol, isFavorite: Bool){
        router.goToDetails(showData: showData, delegate: delegate, isFavorite: isFavorite)
    }
    
    func postAddFavorite(showData:Show){
        interactor?.postAddFavorite(showData:showData)
    }
    
    func postDeleteFavorite(showData:Show){
        interactor?.postDeleteFavorite(showData:showData)
    }
    
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void)){
        router.presentAlert(titleText: titleText, haveCancel: haveCancel, accept: accept)
    }
}

extension ListMoviesPresenter: ListMoviesInteractorOutputProtocol {
    
    func requestShows(data: [Show]) {
        view?.requestShows(data: data)
    }
    
    func requestFavorites(data: [Show]) {
        view?.requestFavorites(data: data)
    }
    
    func responseError(message: String) {
        view?.showError(text: message)
    }
    
    func responseSuccess(message: String) {
        view?.showSuccess(text: message)
    }
    
}
