//
//  MovieTableViewCell.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var imgBillboard: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    //Closure onDetails
    public var onDetails: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupGestures()
    }
    
    //MARK: prepare Gestures of components
    private func setupGestures(){
        viewCard.isUserInteractionEnabled = true
        viewCard.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickedCard(_:))))
        
    }
    //MARK: function if tap card of cell
    @objc func onClickedCard(_ sender: Any) {
        onDetails!()
    }
    
    //MARK: fetch data in cell
    public func setupFor(data:Show){
        imgBillboard.setImage(fromUrl: data.image.medium)
        imgBillboard.makeRoundedWithoutBorder()
        
        lblTitle.text = data.name
        
        var message = clearText(tex: data.summary)
        message = clearTextHTML(tex: message)
        lblDescription.text = clearText(tex: message)
    }
    
}
