//
//  Show.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//

import Foundation

//MARK: Model
struct Show: Codable{
    let id: Int
    let name: String
    let genres: [String]
    let rating: Rating
    let image: ImageBillboard
    let summary: String
    let externals: Externals
    
    var fullGenres: String {
        var text = ""
        for item in self.genres {
            text = text + "\(item)\n"
        }
        return text
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id, name, genres, rating, image, summary, externals
    }
}

struct Rating: Codable{
    let average: Float?
    
    enum CodingKeys: String, CodingKey {
        case average
    }
}

struct ImageBillboard: Codable{
    let medium: String
    let original: String
    
    enum CodingKeys: String, CodingKey {
        case medium, original
    }
}

struct Externals: Codable{
    let imdb: String?
    
    enum CodingKeys: String, CodingKey {
        case imdb
    }
}
