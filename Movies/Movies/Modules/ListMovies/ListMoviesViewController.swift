//
//  ListMoviesViewController.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import UIKit
import DZNEmptyDataSet

class ListMoviesViewController: UIViewController {
    var presenter: ListMoviesPresenterProtocol?
    
    //MARK: Outlets
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnTvShows: UIButton!
    @IBOutlet weak var btnFavorites: UIButton!
    
    //MARK: array to save all data of table
    private var arrayShows: [Show] = []
    
    //MARK: flag is favorite
    private var isFavorite: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTable()
    }
    
    //MARK: getData if is shows or favorites
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !isFavorite {
            presenter?.getShows()
        }else{
            presenter?.getFavorites()
        }
        
    }
    
    //MARK: style of components
    private func setupUI(){
        setupViewWithTab(titleView: "TV Shows")
        changeStyles(btnSelected: btnTvShows, btnUnselected: btnFavorites)
    }
    
    //MARK: prepare table data source, delegate, register custom cell & delegate emptyDataSetSource
    private func setupTable() {
        table.dataSource = self
        table.delegate = self
        table.emptyDataSetSource = self
        table.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
        table.separatorStyle = .none
        
    }
    
    //MARK: function chage title & items Tab Bar if is shows or favorite
    private func changeStyles(btnSelected: UIButton, btnUnselected: UIButton){
        btnUnselected.setTitleColor(.white, for: .normal)
        btnSelected.setTitleColor(.yellow, for: .normal)
        
        if !isFavorite {
            title = "TV Shows"
        }else{
            title = "Favoritos"
        }
    }
    
    
    
    //MARK: select item shows (change style & get data from api )
    @IBAction func onClickedTvShows(_ sender: Any) {
        isFavorite = false
        changeStyles(btnSelected: btnTvShows, btnUnselected: btnFavorites)
        presenter?.getShows()
    }
    
    //MARK: select item shows (change style & get data from local DB )
    @IBAction func onClickedFavorites(_ sender: Any) {
        isFavorite = true
        changeStyles(btnSelected: btnFavorites, btnUnselected: btnTvShows)
        presenter?.getFavorites()
    }
    
    
    
}

extension ListMoviesViewController: ListMoviesViewProtocol {
    
    //MARK: request data from API & reload table
    func requestShows(data: [Show]) {
        self.arrayShows = data
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
    
    //MARK: request data from Local DB & reload table
    func requestFavorites(data: [Show]) {
        self.arrayShows = data
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
    
    //MARK: show alert if get error in getData
    func showError(text: String) {
        presenter?.presentAlert(titleText: text, haveCancel: false, accept:{})
    }
    
    //MARK: show alert if succes changes in local DB & reload data
    func showSuccess(text: String) {
        presenter?.presentAlert(titleText: text, haveCancel: false, accept:{ [self] in
            if !self.isFavorite {
                self.presenter?.getShows()
            }else{
                self.presenter?.getFavorites()
            }
        })
    }
    
    
}
//MARK: extension from table delegates
extension ListMoviesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayShows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
            cell.selectionStyle = .none
        //MARK: injection showData to use in custom cell
            cell.setupFor(data: arrayShows[indexPath.row])
        //MARK: injection closure of cell
            cell.onDetails = {
                self.presenter?.goToDetails(showData: self.arrayShows[indexPath.row], delegate: self, isFavorite: self.isFavorite)
            }
        return cell
    }
    
    //MARK: actions if swipped cell
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        //MARK: swipped delete favorite
        if editingStyle == .delete {
            presenter?.presentAlert(titleText: "¿Desea borrar este show de su lista de favoritos?", haveCancel: true, accept: {
                self.presenter?.postDeleteFavorite(showData: self.arrayShows[indexPath.row])
            })
        }
        
        //MARK: swipped add favorite
        if editingStyle == .insert{
            presenter?.postAddFavorite(showData: self.arrayShows[indexPath.row])
        }
    }
    
    //MARK: Add button swipe if is shows or favorite
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if !isFavorite {
            let favoriteButton = UITableViewRowAction(style: .default, title: "Favorite") { (action, indexPath) in
                self.table.dataSource?.tableView!(self.table, commit: .insert, forRowAt: indexPath)
                return
            }
            favoriteButton.backgroundColor = UIColor.green
            return [favoriteButton]
            
        }else{
            let deleteButton = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
                self.table.dataSource?.tableView!(self.table, commit: .delete, forRowAt: indexPath)
                return
            }
            
            deleteButton.backgroundColor = UIColor.red
            return [deleteButton]
        }
        
        
    }
    
    
}

//MARK: delegate Empty Data Set, include title & description in table if table is empty
extension ListMoviesViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var title: String = ""
        
        if !isFavorite {
            title = "Sin Shows"
        }else{
            title = "Sin Favoritos"
        }
        
        
        let attributedTitle = NSAttributedString(string: title)
        return attributedTitle
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let caption = "No hay información para mostrar"
        let attributedCaption = NSAttributedString(string: caption)
        return attributedCaption
    }
    
}

//MARK: Functions to add/delete of  DetailsShow
extension ListMoviesViewController: DetailsMovieProtocol {
    func addFavorites(showData: Show) {
        presenter?.postAddFavorite(showData: showData)
    }
    
    func deleteFavorites(showData: Show) {
        self.presenter?.postDeleteFavorite(showData: showData)
    }
}
