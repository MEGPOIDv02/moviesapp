//
//  ListMoviesRouter.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation
import UIKit

class ListMoviesRouter {
    weak var viewController: UIViewController?

    //MARK: Function comunication all viper modules & create view for navigation
    static func createModule()->UIViewController{

        let view = ListMoviesViewController(nibName: "ListMoviesViewController", bundle: nil)

        let interactor = ListMoviesInteractor()
        let router = ListMoviesRouter()
        let service = ListMoviesService()
        
        let presenter = ListMoviesPresenter(interface: view, interactor: interactor, router: router)
        
        
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.service = service
        service.interactor = interactor
        router.viewController = view
        
        return view
    }
    
}

extension ListMoviesRouter: ListMoviesRouterProtocol {
    func goToDetails(showData: Show, delegate: DetailsMovieProtocol, isFavorite: Bool) {
        let vc = DetailsMovieRouter.createModule(showData: showData, delegate: delegate, isFavorite: isFavorite)
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void)){
        let vc = AcceptCancelModalRouter.createModule(titleText: titleText, haveCancel: haveCancel, accept:accept)
        viewController!.present(vc, animated: true)
    }
}
    
    
    

