//
//  AcceptCancelModalRouter.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//  
//

import Foundation
import UIKit

class AcceptCancelModalRouter {
    weak var viewController: UIViewController?

    //MARK: Function comunication all viper modules & create view for navigation
    static func createModule(
        titleText: String!,
        haveCancel: Bool?,
        accept: (()->Void)?)->UIViewController{

        let view = AcceptCancelModalViewController(nibName: "AcceptCancelModalViewController", bundle: nil)
        view.modalTransitionStyle = .crossDissolve
        view.modalPresentationStyle = .overCurrentContext
            
        view.titleText = titleText
        view.onAccept = accept
        view.haveCancel = haveCancel
                
            
        let interactor = AcceptCancelModalInteractor()
        let router = AcceptCancelModalRouter()
        let service = AcceptCancelModalService()
        
        let presenter = AcceptCancelModalPresenter(interface: view, interactor: interactor, router: router)
        
        
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.service = service
        service.interactor = interactor
        router.viewController = view
        
        return view
    }
    
}

extension AcceptCancelModalRouter: AcceptCancelModalRouterProtocol {
    
}
    
    
    

