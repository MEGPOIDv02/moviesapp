//
//  AcceptCancelModalProtocols.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//  
//

import Foundation

//View
protocol AcceptCancelModalViewProtocol: AnyObject {
    var presenter: AcceptCancelModalPresenterProtocol? {get set}
}

//Interactor
protocol AcceptCancelModalInteractorInputProtocol: AnyObject {
    //Presenter -> Interactor
    var presenter: AcceptCancelModalInteractorOutputProtocol? {get set}
    var service: AcceptCancelModalServiceInputProtocol? { get set }
}


protocol AcceptCancelModalInteractorOutputProtocol: AnyObject {
    //Interactor->Presenter
}

//Presenter
protocol AcceptCancelModalPresenterProtocol: AnyObject {
    var interactor: AcceptCancelModalInteractorInputProtocol? {get set}
}


//Service
protocol AcceptCancelModalServiceInputProtocol: AnyObject {
    // INTERACTOR -> SERVICE
    var interactor: AcceptCancelModalServiceOutputProtocol? { get set }
}

protocol AcceptCancelModalServiceOutputProtocol: AnyObject {
    // SERVICE -> INTERACTOR
}

//Router
protocol AcceptCancelModalRouterProtocol: AnyObject {
    
}