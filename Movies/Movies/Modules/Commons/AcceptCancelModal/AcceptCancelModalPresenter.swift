//
//  AcceptCancelModalPresenter.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//  
//

import Foundation

class AcceptCancelModalPresenter {
    //MARK: Properties
    var interactor: AcceptCancelModalInteractorInputProtocol?
    weak private var view: AcceptCancelModalViewProtocol?
    private let router: AcceptCancelModalRouterProtocol
    
    //MARK: Dependency injection
    init(interface: AcceptCancelModalViewProtocol, interactor: AcceptCancelModalInteractorInputProtocol, router: AcceptCancelModalRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
}

extension AcceptCancelModalPresenter: AcceptCancelModalPresenterProtocol {
    
}

extension AcceptCancelModalPresenter: AcceptCancelModalInteractorOutputProtocol {
    
}
