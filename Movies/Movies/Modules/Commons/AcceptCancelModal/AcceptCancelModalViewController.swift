//
//  AcceptCancelModalViewController.swift
//  Movies
//
//  Created by Diego on 12/07/23.
//  
//

import UIKit

class AcceptCancelModalViewController: UIViewController {
    var presenter: AcceptCancelModalPresenterProtocol?
    
    //MARK: Outlets
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    //MARK: vars injected in AcceptCancelModalRouter.createModule()
    var titleText: String!
    var haveCancel: Bool? = true
    var onAccept: (()->Void)? = {}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: Style in components view & fetch data
    private func setupUI(){
        viewCard.layer.cornerRadius = 12
        btnAccept.layer.cornerRadius = 12
        btnCancel.layer.cornerRadius = 12
        
        lblTitle.text = titleText
            
        if !haveCancel!{
            btnCancel.isHidden = true
        }
    }
    //MARK: Function Accept
    @IBAction func onClickedAccept(_ sender: Any) {
        dismiss(animated: true) {
            self.onAccept!()
        }
    }
    
    //MARK: Function Cancel
    @IBAction func onClickedCancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
}

extension AcceptCancelModalViewController: AcceptCancelModalViewProtocol {
    
}

