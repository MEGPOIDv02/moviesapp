//
//  DetailsMovieViewController.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import UIKit
protocol DetailsMovieProtocol {
    func addFavorites(showData: Show)
    func deleteFavorites(showData: Show)
}

class DetailsMovieViewController: UIViewController {
    var presenter: DetailsMoviePresenterProtocol?
    
    //MARK: Outlets
    @IBOutlet weak var imgBillboard: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgHeart: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var btnWebSite: UIButton!
    
    
    //MARK: vars injected in DetailsMovieRouter.createModule()
    var showData: Show!
    var delegate: DetailsMovieProtocol?
    var isFavorite: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        fetchData()
    }
    
    //MARK: Style in components view & fetch data
    private func setupUI(){
        setupView(titleView: "\(showData.name)")
        btnWebSite.layer.cornerRadius = 12
        imgBillboard.layer.cornerRadius = 12
        setupGestures()
        
        if isFavorite {
            imgHeart.image = UIImage(named: "ic.heart.fill")
        }else{
            imgHeart.image = UIImage(named: "ic.heart")
        }
    }
    
    //MARK: prepare Gestures of components
    private func setupGestures(){
        imgHeart.isUserInteractionEnabled = true
        imgHeart.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickedHeart(_:))))
    }
    
    //MARK: Fetch Data of Shows
    private func fetchData(){
        DispatchQueue.main.async {
            self.imgBillboard.setImage(fromUrl: self.showData.image.original)
        }
        
        
        lblTitle.text = showData.name
        
        var message = clearText(tex: showData.summary)
        message = clearTextHTML(tex: message)
        lblDescription.text = clearText(tex: message)
        
        if let average = showData.rating.average{
            lblRating.text = "Rating: \(average)"
        }else{
            lblRating.isHidden = true
        }
        
        lblGenres.text = showData.fullGenres
        
        if showData.externals.imdb != nil {
            btnWebSite.isHidden = false
        }
    }
    
    
    //MARK: function open safari with url
    @IBAction func onClickedWebSite(_ sender: Any) {
        if let url = URL(string: "\(ApiEndpoint.imdb.rawValue)\(showData.externals.imdb!)/") {
            UIApplication.shared.open(url)
        }
    }
    
    //MARK: Function change image heart & add/remove favorite
    @objc func onClickedHeart(_ sender: Any) {
        if !isFavorite {
            imgHeart.image = UIImage(named: "ic.heart.fill")
            delegate?.addFavorites(showData: self.showData)
            self.isFavorite = !isFavorite
        }else{
            
            presenter?.presentAlert(titleText: "¿Desea borrar este show de su lista de favoritos?", haveCancel: true, accept: {
                self.imgHeart.image = UIImage(named: "ic.heart")
                self.delegate?.deleteFavorites(showData: self.showData)
                self.isFavorite = !self.isFavorite
            })
        }
        
    }
    
}

extension DetailsMovieViewController: DetailsMovieViewProtocol {
    
}

