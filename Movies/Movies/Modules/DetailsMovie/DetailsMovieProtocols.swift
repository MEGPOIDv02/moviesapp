//
//  DetailsMovieProtocols.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation

//View
protocol DetailsMovieViewProtocol: AnyObject {
    var presenter: DetailsMoviePresenterProtocol? {get set}
}

//Interactor
protocol DetailsMovieInteractorInputProtocol: AnyObject {
    //Presenter -> Interactor
    var presenter: DetailsMovieInteractorOutputProtocol? {get set}
    var service: DetailsMovieServiceInputProtocol? { get set }
}


protocol DetailsMovieInteractorOutputProtocol: AnyObject {
    //Interactor->Presenter
}

//Presenter
protocol DetailsMoviePresenterProtocol: AnyObject {
    var interactor: DetailsMovieInteractorInputProtocol? {get set}
    //MARK: Function go to AcceptCancelModal
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void))
}


//Service
protocol DetailsMovieServiceInputProtocol: AnyObject {
    // INTERACTOR -> SERVICE
    var interactor: DetailsMovieServiceOutputProtocol? { get set }
}

protocol DetailsMovieServiceOutputProtocol: AnyObject {
    // SERVICE -> INTERACTOR
}

//Router
protocol DetailsMovieRouterProtocol: AnyObject {
    //MARK: Function go to AcceptCancelModal
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void))
}
