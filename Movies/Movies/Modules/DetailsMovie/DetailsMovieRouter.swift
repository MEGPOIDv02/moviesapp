//
//  DetailsMovieRouter.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation
import UIKit

class DetailsMovieRouter {
    weak var viewController: UIViewController?

    //MARK: Function comunication all viper modules & create view for navigation
    static func createModule(showData: Show, delegate: DetailsMovieProtocol, isFavorite: Bool)->UIViewController{

        let view = DetailsMovieViewController(nibName: "DetailsMovieViewController", bundle: nil)
        view.showData = showData
        view.delegate = delegate
        view.isFavorite = isFavorite

        let interactor = DetailsMovieInteractor()
        let router = DetailsMovieRouter()
        let service = DetailsMovieService()
        
        let presenter = DetailsMoviePresenter(interface: view, interactor: interactor, router: router)
        
        
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.service = service
        service.interactor = interactor
        router.viewController = view
        
        return view
    }
    
}

extension DetailsMovieRouter: DetailsMovieRouterProtocol {
    //MARK: function Navigation go to AcceptCancelModal
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void)){
        let vc = AcceptCancelModalRouter.createModule(titleText: titleText, haveCancel: haveCancel, accept:accept)
        viewController!.present(vc, animated: true)
    }
}
    
    
    

