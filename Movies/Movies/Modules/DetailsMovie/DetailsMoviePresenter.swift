//
//  DetailsMoviePresenter.swift
//  Movies
//
//  Created by Diego on 11/07/23.
//  
//

import Foundation

class DetailsMoviePresenter {
    //MARK: Properties
    var interactor: DetailsMovieInteractorInputProtocol?
    weak private var view: DetailsMovieViewProtocol?
    private let router: DetailsMovieRouterProtocol
    
    //MARK: Dependency injection
    init(interface: DetailsMovieViewProtocol, interactor: DetailsMovieInteractorInputProtocol, router: DetailsMovieRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
}

extension DetailsMoviePresenter: DetailsMoviePresenterProtocol {
    //MARK: function go to router to present AcceptCancelModal
    func presentAlert(titleText: String!, haveCancel: Bool, accept:@escaping (()->Void)){
        router.presentAlert(titleText: titleText, haveCancel: haveCancel, accept: accept)
    }
}

extension DetailsMoviePresenter: DetailsMovieInteractorOutputProtocol {
    
}
