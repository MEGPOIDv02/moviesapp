//
//  MoviesTests.swift
//  MoviesTests
//
//  Created by Diego on 11/07/23.
//

import XCTest
@testable import Movies
import CoreData

class MoviesTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //MARK: function test EndPoints if exists
    func testEndPoint() throws {
        let link: String = ApiEndpoint.shows.rawValue
//        let link: String = ApiEndpoint.imdb.rawValue
//        let link: String = "asdasdasd"
        
        if verifyUrl(urlString: link) {
                XCTAssertTrue(true, "Correct link")
        }else{
            XCTAssertTrue(false, "There was a problem with your link")
        }
    }

    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }

}
